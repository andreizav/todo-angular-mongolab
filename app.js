var app = angular.module('task', ['mongolab']).
    config(function($routeProvider) {
        $routeProvider.
            when('/list', {controller:'ListCtrl', template:'list.html'}).
            when('/edit/:taskId', {controller:'EditCtrl', template:'detail.html'}).
            when('/new', {controller:'CreateCtrl', template:'detail.html'}).
            when('/view/:taskId', {controller:'ViewCtrl', template:'view.html'}).
            otherwise({redirectTo:'/list'});
    });

app.controller('ViewCtrl', function($scope, $location, $routeParams, Task) {
    Task.get({id: $routeParams.taskId}, function(task) {
        self.original = task;
        $scope.task = new Task(self.original);
    });
});

app.controller('ListCtrl', function($scope, Task) {
    $scope.tasks = Task.query();
});

app.controller('CreateCtrl', function($scope, $location, Task) {
    $scope.save = function() {
        Task.save($scope.task, function(task) {
            $location.path('/list/');
        });
    }
});

app.controller('EditCtrl', function($scope, $location, $routeParams, Task) {
    var self = this;

    Task.get({id: $routeParams.taskId}, function(task) {
        self.original = task;
        $scope.task = new Task(self.original);
    });

    $scope.destroy = function() {
        self.original.destroy(function() {
            $location.path('/list');
        });
    };

    $scope.save = function() {
        $scope.task.update(function() {
            $location.path('/list');
        });
    };
});

// This is a module for cloud persistance in mongolab - https://mongolab.com
angular.module('mongolab', ['ngResource']).
    factory('Task', function($resource) {
        var Task = $resource('https://api.mongolab.com/api/1/databases' +
                '/todo/collections/tasks/:id',
            { apiKey: 'MWxF-8RKg5wx4oLLDDDZa5Mdd01to7zV' }, {
                update: { method: 'PUT' }
            }
        );

        Task.prototype.update = function(cb) {
            return Task.update({id: this._id.$oid},
                angular.extend({}, this, {_id:undefined}), cb);
        };

        Task.prototype.destroy = function(cb) {
            return Task.remove({id: this._id.$oid}, cb);
        };

        return Task;
    });

